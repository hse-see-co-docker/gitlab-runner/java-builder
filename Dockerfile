FROM gitlab-registry.cern.ch/hse-see-co-docker/openjdk-cern:latest

ARG MAVEN_VERSION=3.5.4
ARG USER_HOME_DIR="/home/builder"
ARG SHA=ce50b1c91364cb77efe3776f756a6d92b76d9038b0a0782f7d53acf1e997a14d
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha256sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

ENV GRADLE_HOME /opt/gradle
ENV GRADLE_VERSION 4.10.2

ARG GRADLE_DOWNLOAD_SHA256=b49c6da1b2cb67a0caf6c7480630b51c70a11ca2016ff2f555eaeda863143a29
RUN set -o errexit -o nounset \
	&& echo "Downloading Gradle" \
	&& curl -fsSL -o gradle.zip "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" \
	\
	&& echo "Checking download hash" \
	&& echo "${GRADLE_DOWNLOAD_SHA256} *gradle.zip" | sha256sum --check - \
	\
	&& echo "Installing Gradle" \
	&& unzip gradle.zip \
	&& rm gradle.zip \
	&& mv "gradle-${GRADLE_VERSION}" "${GRADLE_HOME}/" \
	&& ln --symbolic "${GRADLE_HOME}/bin/gradle" /usr/bin/gradle

RUN echo "Adding user and group builder for maven/gradle" \
	&& groupadd --system --gid 1000 builder \
	&& useradd --system --gid builder --uid 1000 --shell /bin/bash --create-home builder \
	&& mkdir $USER_HOME_DIR/.gradle \
	&& mkdir $USER_HOME_DIR/.m2 \
	&& chown --recursive builder:builder $USER_HOME_DIR \
	&& chmod u+rw --recursive $USER_HOME_DIR \
	\
	&& echo "Symlinking root Gradle cache to gradle Gradle cache" \
	&& ln -s /home/builder/.gradle /root/.gradle \
	&& ln -s /home/builder/.gradle /root/.m2

RUN echo javax.xml.accessExternalSchema = all>>$JAVA_HOME/jre/lib/jaxp.properties
# Create Gradle volume
USER builder
VOLUME "/home/builder/.gradle"
VOLUME "/home/builder/.m2"
WORKDIR /home/builder

RUN set -o errexit -o nounset \
	&& echo "Testing installation" \
	&& gradle --version \
	&& mvn --version
